<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\GameRound;
use AppBundle\Utils\Turn;

class GameController extends Controller
{
    const WINNER_KEY = 'winner';
    const PLAYER_KEY = 'player';
    const TIE_KEY = 'tie';

    /**
     * @Route("/", name="play")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        // Retrieve our list of game actions
        $actions = $this->get('app.actions')->getActions();

        return $this->render('AppBundle:Game:index.html.twig', array(
            'game' => array(
                'actions' => $actions
            )
        ));
    }

    /**
     * @Route("/results/{action}", name="results")
     *
     * @param $action
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function playAction($action)
    {
        // Retrieve our Actions service
        $actions = $this->get('app.actions');
        // Build a new Turn object with game parameters
        $turn = new Turn( $actions, $action );

        $playerChoice = $action; // Player action is passed via url parameter
        $computerChoice = $turn->getCompTurn(); // Comp action is randomly chosen

        $winner = $turn->getWinner(); // Get our winner, or declare a tie
        $message = $turn->getMessage(); // Get the message associated with the win action v lose action

        // Bypass GameRoundRepository overriding entity getters and setters
        $gameRound = new GameRound();
        $gameRound->setPlayerAction( $action );
        $gameRound->setComputerAction( $computerChoice );
        $gameRound->setWinner( $winner );
        /**
         * TODO: If this application were under heavy use or load, we would want this persistence
         * handled by an event or response listener. This would avoid saving to the db if we error out on the
         * TWIG rendering, allowing the match to be replayed and results more accurate to rounds
         * truly played.
         */

        // Save our GameRound data to the database
        $em = $this->getDoctrine()->getManager();
        $em->persist( $gameRound );
        $em->flush();

        return $this->render('AppBundle:Game:play.html.twig', array(
            'game' => array(
                'winner'        =>  $winner,
                'message'       =>  $message,
                'player'        =>  $playerChoice,
                'computer'      =>  $computerChoice,
                'actions'       =>  $actions->getActions()
            )
        ));
    }

    /**
     * @Route("/scores", name="scores")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function scoresAction() {

        // Retrieve our list of game actions
        $actions = $this->get('app.actions');
        $actionsList = $actions->getActions();

        // Get our ScoreCard service
        $scoreCard = $this->get('app.scorecard');

        $actionsList = $scoreCard->filterOrder( $actionsList );

        // Get number of all matches played
        $allPlayed = $scoreCard->getAllPlayedCount();
        // Get all actions and number of times they were played
        $actionPlays = $scoreCard->getAllActionPlayCounts();
        // Get all actions and number of times they were played for a win
        $actionWins = $scoreCard->getAllActionWinCounts();

        // Get number of times all actions were played by player
        $playerActions = $scoreCard->getAllActionCountsForPlayer();
        // Get number of times all actions were played by computer
        $computerActions = $scoreCard->getAllActionCountsForComputer();

        // Get number of times all actions were played for a win by player
        $playerWinActions = $scoreCard->getAllWinActionCountsForPlayer();
        // Get number of times all actions were played for a win by computer
        $computerWinActions = $scoreCard->getAllWinActionCountsForComputer();

        // Get total number of player wins
        $playerWins = $scoreCard->getWinCountForPlayer();
        // Get total number of computer wins
        $computerWins = $scoreCard->getWinCountForComputer();

        // Get number of tie games
        $tieCount = $scoreCard->getAllTieCount();
        // Get number of tie games for all actions
        $actionTies = $scoreCard->getTieCountsForActions();

        return $this->render('AppBundle:Game:scores.html.twig', array(
            'game' => array(
                'actions'           => $actionsList,
                'actionPlays'       => $actionPlays,
                'actionWins'        => $actionWins,
                'actionTies'        => $actionTies,
                'playerActions'     => $playerActions,
                'compActions'       => $computerActions,
                'playerWinActions'  => $playerWinActions,
                'compWinActions'    => $computerWinActions,
                'allPlayed'         => $allPlayed,
                'ties'              => $tieCount,
                'playerWins'        => $playerWins,
                'compWins'          => $computerWins
            )
        ));
    }
}
