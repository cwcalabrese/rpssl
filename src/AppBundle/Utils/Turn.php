<?php

namespace AppBundle\Utils;

use AppBundle\Service\Actions;

class Turn
{
    protected $actions;

    protected $playerTurn;

    protected $compTurn;

    /**
     * Turn constructor.
     * @param Actions $actions
     * @param $action
     */
    public function __construct( Actions $actions, $action )
    {
        $this->actions = $actions;
        $this->compTurn = self::takeCompTurn();
        $this->playerTurn = $action;
    }

    // Private constructor methods

    /**
     * @return mixed
     */
    private function takeCompTurn()
    {
        $compActionPos = mt_rand ( 0, 4 );

        return $this->actions->getActionFromPosition( $compActionPos );
    }

    /**
     * @return string
     */
    public function getWinner()
    {
        $winner = 'computer';

        $playerAdv = $this->actions->getActionAdvantages( $this->playerTurn );
        $compAdv = $this->actions->getActionAdvantages( $this->compTurn );

        $playerWin = in_array ( $this->compTurn, $playerAdv );
        $compWin = in_array ( $this->playerTurn, $compAdv );

        if ( $playerWin == $compWin ) {
            $winner = 'tie';
        } else if ( $playerWin ) {
            $winner = 'player';
        }

        return $winner;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        $winner = self::getWinner();

        if ( $winner == 'tie' ) {
            return self::getTieMessage();
        }

        $playerWon = ( $winner == 'player' );

        $winAction = $playerWon
            ? $this->playerTurn
            : $this->compTurn;

        $loseAction = $playerWon
            ? $this->compTurn
            : $this->playerTurn;

        $message = $this->actions->getActionWinMsg( $winAction, $loseAction );

        return $message;
    }

    /**
     * @return string
     */
    private function getTieMessage()
    {
        return 'The match is a draw.';
    }

    public function getPlayerTurn()
    {
        return $this->playerTurn;
    }

    public function getCompTurn()
    {
        return $this->compTurn;
    }
}