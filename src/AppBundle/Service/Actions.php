<?php

namespace AppBundle\Service;

class Actions {

    // Define our constant values for our actions array
    const ACT_MAX_FOR_LOOP = 4;
    const FIRST_ADV_POS = 1;
    const SECOND_ADV_POS = 3;
    const ADJ_FOR_COUNT = 1; // Adjusts for zero index regarding key v count

    /**
     * @param $action
     * @return array
     */
    public function getActionAdvantages( $action )
    {
        $action = strtolower ( $action );

        $actions = self::getActions();

        $pos = array_search ( $action, $actions );

        $adv1 = self::getAdvantage( $pos, self::FIRST_ADV_POS );
        $adv2 = self::getAdvantage( $pos, self::SECOND_ADV_POS );

        return array(
            $adv1,
            $adv2
        );
    }

    /**
     * @param $pos
     * @param $adv
     * @return mixed
     */
    private function getAdvantage( $pos, $adv )
    {
        $pointer = self::adjustPosForLoop( $pos + $adv );
        $actions = self::getActions();

        return $actions[$pointer];
    }

    /**
     * @param $winner
     * @param $loser
     * @return string
     */
    public function getActionWinMsg( $winner, $loser )
    {
        $messages = self::getMessages();
        $messagePos = 0; // Default our win message to first index of messages array

        // Turn our winner and loser values in numerical values
        $winnerPos = self::getPositionFromAction( $winner );
        $loserPos = self::getPositionFromAction( $loser );

        $diff = $winnerPos - $loserPos;

        $diffPos = $diff < 0
            ? $diff + 5 // rule of five to adjust for loop
            : $diff;

        // If the advantage was for the second index, we update here
        if ( ( $diffPos + self::ADJ_FOR_COUNT ) == self::SECOND_ADV_POS ) { // Adjust diffPos to match count
            $messagePos = 1;
        }

        $actionKey = self::adjustPosForLoop( $winnerPos );

        $messageLink = $messages[$actionKey][$messagePos];

        $message = ucfirst ( $winner ) . ' '
            . $messageLink . ' '
            . ucfirst ( $loser );

        return $message;
    }

    /**
     * @return array
     */
    public function getActions()
    {
        return array(
            'rock',
            'lizard',
            'spock',
            'scissors',
            'paper'
        );
    }

    /**
     * @return array
     */
    private function getMessages()
    {
        return array(
            // Win messages for Rock action.
            /**
             * We could include a catch for message arrays that use the same
             * message for both win cases, but as Rock is the only case in which
             * this would occur, we repeat the string for it's message here.
             */
            array(
                'crushes',      // Lizard
                'crushes'       // Scissors
            ),
            // Win messages for Lizard action.
            array(
                'poisons',      // Spock
                'eats'          // Paper
            ),
            // Win messages for Spock action.
            array(
                'smashes',      // Scissors
                'vaporizes'     // Rock
            ),
            // Win messages for Scissors action.
            array(
                'cuts',         // Paper
                'decapitates'   // Lizard
            ),
            // Win messages for Paper action.
            array(
                'covers',       // Rock
                'disproves'     // Spock
            )
        );
    }

    // Utility methods

    /**
     * @param $pos
     * @return mixed
     */
    public function adjustPosForLoop( $pos )
    {
        $returnPosition = $pos;

        if ( $pos > self::ACT_MAX_FOR_LOOP ) {
            $returnPosition = $pos - ( self::ACT_MAX_FOR_LOOP + self::ADJ_FOR_COUNT ); // Adjust max for count
        }

        return $returnPosition;
    }

    /**
     * @param $pos
     * @return mixed
     */
    public function getActionFromPosition( $pos )
    {
        $actions = self::getActions();

        return $actions[$pos];
    }


    /**
     * @param $action
     * @return mixed
     */
    public function getPositionFromAction( $action )
    {
        $actions = self::getActions();
        $position = array_search ( $action, $actions );

        return $position;
    }
}