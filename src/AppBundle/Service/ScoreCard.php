<?php

namespace AppBundle\Service;

use AppBundle\Entity\GameRoundRepository;

class ScoreCard
{
    const PLAYER = 'player';
    const COMP = 'computer';

    private $gameRound;

    private $actions;

    /**
     * ScoreCard constructor.
     * @param GameRoundRepository $gameRound
     * @param Actions $actions
     */
    public function __construct( GameRoundRepository $gameRound, Actions $actions )
    {
        $this->gameRound = $gameRound;
        $this->actions = $actions->getActions();
    }

    public function getAllPlayedCount()
    {
        return count ( $this->gameRound->findAll() );
    }

    /**
     * @return mixed
     */
    public function getAllActionPlayCounts()
    {
        $returnArr = array();

        foreach ( $this->actions as $action ) {

            $returnPlayerAction = $this->gameRound->findActionsByPlayer( 'player', $action );
            $returnCompAction = $this->gameRound->findActionsByPlayer( 'computer', $action);

            $returnAction = ( count ( $returnPlayerAction ) + count ( $returnCompAction ));

            $returnArr[$action] = $returnAction;
        }

        return self::filterOrder( $returnArr );
    }

    /**
     * @return mixed
     */
    public function getAllActionWinCounts()
    {
        $returnArr = array();

        foreach ( $this->actions as $action ) {

            $returnAction = $this->gameRound->findActionsByWin( $action );

            $returnArr[$action] = count ( $returnAction );
        }

        return self::filterOrder( $returnArr );
    }

    /**
     * @return mixed
     */
    public function getAllActionCountsForPlayer()
    {
        $returnArr = array();
        $player = self::PLAYER;

        foreach ( $this->actions as $action ) {

            $returnAction = $this->gameRound->findActionsByPlayer( $player, $action );

            $returnArr[$action] = count ( $returnAction );
        }

        return self::filterOrder( $returnArr );
    }

    /**
     * @return mixed
     */
    public function getAllActionCountsForComputer()
    {
        $returnArr = array();
        $player = self::COMP;

        foreach ( $this->actions as $action ) {

            $returnAction = $this->gameRound->findActionsByPlayer( $player, $action );

            $returnArr[$action] = count ( $returnAction );
        }

        return self::filterOrder( $returnArr );
    }

    /**
     * @return int
     */
    public function getWinCountForPlayer()
    {
        $player = self::PLAYER;

        $returnCount = count ( $this->gameRound->findPlayerByWin( $player ));

        return $returnCount;
    }

    /**
     * @return int
     */
    public function getWinCountForComputer()
    {
        $player = self::COMP;

        $returnCount = count ( $this->gameRound->findPlayerByWin( $player ));

        return $returnCount;
    }

    /**
     * @return mixed
     */
    public function getAllWinActionCountsForPlayer()
    {
        $returnArr = array();
        $player = self::PLAYER;

        foreach ( $this->actions as $action ) {

            $returnAction = $this->gameRound->findPlayerWinByAction( $player, $action );

            $returnArr[$action] = count ( $returnAction );
        }

        return self::filterOrder( $returnArr );
    }

    /**
     * @return mixed
     */
    public function getAllWinActionCountsForComputer()
    {
        $returnArr = array();
        $player = self::COMP;

        foreach ( $this->actions as $action ) {

            $returnAction = $this->gameRound->findPlayerWinByAction( $player, $action );

            $returnArr[$action] = count ( $returnAction );
        }

        return self::filterOrder( $returnArr );
    }

    /**
     * @return int
     */
    public function getAllTieCount()
    {
        return count ( $this->gameRound->findBy(array(
            'winner' => 'tie'
        )));
    }

    /**
     * @return mixed
     */
    public function getTieCountsForActions()
    {
        $returnArr = array();

        foreach ( $this->actions as $action ) {

            $returnAction = $this->gameRound->findTieByAction( $action );

            $returnArr[$action] = count ( $returnAction );
        }

        return self::filterOrder( $returnArr );
    }

    // Puplic utility methods

    /**
     * @param $actionArr
     * @return mixed
     */
    public function filterOrder( $actionArr )
    {
        $order = array ( 0, 4, 3, 2, 1 );

        array_multisort( $order, $actionArr );

        return $actionArr;
    }
}