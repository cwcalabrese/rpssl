<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class GameRoundRepository extends EntityRepository
{
    /**
     * @param $player
     * @param $action
     * @return array
     */
    public function findActionsByPlayer( $player, $action )
    {
        $query = $this->getEntityManager();
        $playerField = $player . '_action';

        $returnQuery = null;

        return $query->createQuery(
            'SELECT m.' . $playerField . ' FROM AppBundle:GameRound m 
              WHERE m.' . $playerField . ' = :action'
        )->setParameters(
            array(
                'action' => $action
            )
        )->getResult();
    }

    /**
     * @param $action
     * @return array
     */
    public function findActionsByWin( $action )
    {
        $query = $this->getEntityManager();

        return $query->createQuery(
            'SELECT m FROM AppBundle:GameRound m 
              WHERE (
                m.player_action = :action
                AND m.winner = :player ) 
              OR (
                m.computer_action = :action
                AND m.winner = :computer )'
        )->setParameters(
            array(
                'action' => $action,
                'player' => 'player',
                'computer' => 'computer'
            )
        )->getResult();
    }

    public function findTieByAction( $action )
    {
        $query = $this->getEntityManager();

        return $query->createQuery(
            'SELECT m FROM AppBundle:GameRound m 
              WHERE (
                m.winner = :winner
                AND m.player_action = :action )' // We only grab player_action since it's a tie and both will be the same.
        )->setParameters(
            array(
                'winner' => 'tie',
                'action' => $action
            )
        )->getResult();
    }

    public function findPlayerByWin( $player )
    {
        $query = $this->getEntityManager();
        $playerField = $player . '_action';

        return $query->createQuery(
            'SELECT m.' . $playerField . ' FROM AppBundle:GameRound m 
              WHERE m.winner = :winner'
        )->setParameters(
            array(
                'winner' => $player
            )
        )->getResult();
    }

    public function findPlayerWinByAction( $player, $action )
    {
        $query = $this->getEntityManager();
        $playerField = $player . '_action';

        return $query->createQuery(
            'SELECT m.' . $playerField . ' FROM AppBundle:GameRound m 
              WHERE ( 
               m.winner = :player
               AND m.' . $playerField . ' = :action )'
        )->setParameters(
            array(
                'player' => $player,
                'action' => $action
            )
        )->getResult();
    }
}
