<?php

namespace AppBundle\Entity;

/**
 * GameRound
 */
class GameRound
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $played_on;

    /**
     * @var string
     */
    private $player_action;

    /**
     * @var string
     */
    private $computer_action;

    /**
     * @var string
     */
    private $winner;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set playedOn
     *
     * @param \DateTime $playedOn
     *
     * @return GameRound
     */
    public function setPlayedOn($playedOn)
    {
        $this->played_on = $playedOn;

        return $this;
    }

    /**
     * Get playedOn
     *
     * @return \DateTime
     */
    public function getPlayedOn()
    {
        return $this->played_on;
    }

    /**
     * Set playerAction
     *
     * @param string $playerAction
     *
     * @return GameRound
     */
    public function setPlayerAction($playerAction)
    {
        $this->player_action = $playerAction;

        return $this;
    }

    /**
     * Get playerAction
     *
     * @return string
     */
    public function getPlayerAction()
    {
        return $this->player_action;
    }

    /**
     * Set computerAction
     *
     * @param string $computerAction
     *
     * @return GameRound
     */
    public function setComputerAction($computerAction)
    {
        $this->computer_action = $computerAction;

        return $this;
    }

    /**
     * Get computerAction
     *
     * @return string
     */
    public function getComputerAction()
    {
        return $this->computer_action;
    }

    /**
     * Set winner
     *
     * @param string $winner
     *
     * @return GameRound
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;

        return $this;
    }

    /**
     * Get winner
     *
     * @return string
     */
    public function getWinner()
    {
        return $this->winner;
    }
}

