RPSSL
=====

Rock, Paper, Scissors, Spock, Lizard

a Symfony2 sample application based on the Law of Fives.

DEPENDENCIES
============

The RPSSL application relies on the following dependencies using the
composer package management service found here:
[Get Composer](http://getcomposer.org)

Note: If windows installation of composer fails, download composer.phar file
and place it in the root of the application for use as `php composer.phar`

- PHP 5.3.9 or higher (PHP 7.0.9 was used on this project)
- Symfony v2.8 (stable, maintained) full project
- Stof Doctrine Extensions Bundle v1.2
- Symfony Assetic Bundle v2.8

And for development (not required, but used)

- Symfony VarDumper

Note: All Symfony2 standard bundles are assumed with Symfony full project

INSTALLATION
============

**Local Installation**

_Using MAMP or stand-alone LAMP stack server_

Download MAMP for Mac or Windows here:
[Get MAMP](https://www.mamp.info/en/)

Follow installation directions for MAMP, then continue.

1. Clone repository from Bitbucket [repository link](https://bitbucket.org/cwcalabrese/rpssl)

2. Enable and add vhost entry with web root set as root

3. Via bash terminal, enter the following command:
```
    php composer install
```
4. Once composer has installed all vendors and bundles, navigate the code to app/config/parameters.yml and update database parameters.

5. Again with terminal, enter the following command:
```
    php app\console doctrine:database:create
```
6. Once you see the message that the database connection was made, enter the following command to build our table
```
    php app\console doctrine:schema:update --force
```
7. When you see the message that 1 query was executed, open your web browser and navigate to the url used in your vhost entry

8. Enjoy playing Paper, Rock, Scissors, Spock, Lizard.

9. Click 'view scores' after your first round to begin watching the scores as you play